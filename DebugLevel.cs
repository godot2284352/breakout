using System.Collections.Generic;
using System.Linq;
using Godot;

namespace Breakout;
public static class getDebugLevel
{
	internal static LevelInfo GetDebugLevel()
	{
		var lvl = new int[][] {
			new int[]{ 0,0,0,0,0,0,0,0,0,0,0,0 },
			new int[]{ 0,0,5,0,0,0,0,0,0,0,0,0 },
			new int[]{ 0,0,0,0,7,5,0,0,0,0,7,0 },
			new int[]{ 0,0,0,0,0,0,0,0,0,0,0,0 },
			new int[]{ 0,0,0,0,0,0,0,0,0,0,0,0 },
			new int[]{ 1,3,4,7,6,8,8,8,5,4,3,2 },
		};

		var lvlRows = new List<BlockInfo[]>();
		var colors = new Color[] { Colors.White, Colors.LightSeaGreen, Colors.OrangeRed, Colors.SkyBlue };

		foreach (var row in lvl)
		{
			var rowColor = colors[GD.RandRange(0, colors.Length - 1)];
			var lvlCols = row.Select(c => new BlockInfo((BlockTypes)c, rowColor));

			lvlRows.Add(lvlCols.ToArray());
		}

		var lvlInfo = new LevelInfo()
		{
			Name = "Happy Debug",
			Blocks = lvlRows.ToArray(),
		};

		return lvlInfo;
	}

}