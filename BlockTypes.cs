﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Breakout
{
	public enum BlockTypes
	{
		Normal = 0,
		Acceleration = 1,
		Deceleration = 2,
		Rocket = 3,
		BallMagnet = 4,
		Bomb = 5,
		Random = 6,
		Indestructible = 7,
		Petrified2 = 8,
		Petrified1 = 9,
	}

	public static class BlockTypesWithDropsWithoutRandom
	{
		public static List<BlockTypes> Types = new(){
			// BlockTypes.Acceleration,
			// BlockTypes.Deceleration,
			BlockTypes.Rocket,
			BlockTypes.BallMagnet
		};
	}

}
