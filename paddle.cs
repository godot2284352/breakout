using Breakout;
using Godot;
using System;
using System.Collections.Generic;
using System.Data;
using static Godot.CameraFeed;

public partial class paddle : StaticBody2D
{
	[Export]
	public float StartVelocity { get; set; } = 300.0f;

	[Export]
	public Vector2 DefaultDirection = new Vector2(0.0f, -300.0f);
	// public static Vector2 DefaultDirection = new Vector2(StartVelocity / 2.0f, StartVelocity);

	[Export]
	public float TargetLineLength { get; set; } = 100.0f;



	public float StartPositionY { get; private set; }
	public float PaddleWidth { get; private set; }
	public float PaddleWidthHalf { get; private set; }
	public float BallWidthHalf { get; private set; }
	public float BallHeight { get; private set; }

	public BlockTypes? IsGoodieActive { get; private set; }

	public Timer RemoveGoodieTimer { get; private set; }

	public Vector2 CenterPosition => new(Position.X + PaddleWidthHalf, Position.Y);

	private readonly List<Node2D> allGoodieNodes = new();
	private Vector2 localMiddlePoint;
	private ball startingBall;
    private PackedScene rocketSzene;
	private float rocketSizeHalf;

	public override void _Ready()
	{
		var ballSize = ResourceLoader.Load<PackedScene>("res://ball.tscn").Instantiate().GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size;

		StartPositionY = Position.Y;
		PaddleWidth = GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size.X;
		PaddleWidthHalf = PaddleWidth / 2.0f;
		BallWidthHalf = ballSize.X / 2.0f;
		BallHeight = ballSize.Y;
		rocketSzene = ResourceLoader.Load<PackedScene>("res://rocket.tscn");
		rocketSizeHalf = GetNode<Sprite2D>("Rockets/Left").GetRect().Size.X / 2;

		var paddleHeight = GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size.Y;
		localMiddlePoint = new Vector2(PaddleWidthHalf, paddleHeight / 2.0f);

		RemoveGoodieTimer = GetNode<Timer>("RemoveGoodiesTimer");
		RemoveGoodieTimer.Timeout += RemoveGoodieTimer_Elapsed;

		allGoodieNodes.Add(GetNode<Node2D>("Rockets"));
		allGoodieNodes.Add(GetNode<Node2D>("Magnets"));

		move();
	}

	public override void _UnhandledInput(InputEvent @event)
	{
		if (@event is InputEventMouseMotion)
		{
			if (Input.IsActionPressed("BallschussZielen") && startingBall != null)
			{
				QueueRedraw();
			}
			else
			{
				move();
			}
		}

		if (Input.IsActionJustReleased("BallschussZielen"))
		{
			QueueRedraw();
			move();
		}

		if (Input.IsActionJustPressed("BallSchiessen") && (startingBall != null))
		{
			QueueRedraw();

			if (Input.IsActionPressed("BallschussZielen"))
			{
				var ballSpeed = StartVelocity;
				var lineDirectionNormalized = (GetGlobalMousePosition() - CenterPosition).Normalized();
				var direction = ballSpeed * lineDirectionNormalized;

				startingBall.Start(direction);
			}
			else
			{
				startingBall.Start(DefaultDirection);
			}
			startingBall = null;
		}

		if (Input.IsActionJustPressed("GoodieAbschiessen") && IsGoodieActive != null)
		{
			switch (IsGoodieActive.Value) {
				case BlockTypes.Rocket:
					GD.Print("Schieße Rakete!!!");

					var rocketLeft = rocketSzene.Instantiate<Rocket>();
					rocketLeft.Position = new Vector2(Position.X - rocketSizeHalf, Position.Y);
					GetParent().AddChild(rocketLeft);

					var rocketRight = rocketSzene.Instantiate<Rocket>();
					rocketRight.Position = new Vector2(Position.X + PaddleWidth + rocketSizeHalf, Position.Y);
					GetParent().AddChild(rocketRight);
				break;
			}
		}	}

	public override void _Draw()
	{
		if (Input.IsActionPressed("BallschussZielen") && startingBall != null)
		{
			var lineDirectionNormalized = (GetLocalMousePosition() - localMiddlePoint).Normalized();
			var endPointStartDirectionLine = localMiddlePoint + (TargetLineLength * lineDirectionNormalized);

			DrawDashedLine(localMiddlePoint, endPointStartDirectionLine, Colors.HotPink);
		}
	}


	public void move()
	{
		Position = new Vector2(GetGlobalMousePosition().X - PaddleWidthHalf, StartPositionY);

		if (startingBall != null)
		{
			startingBall.Position = new Vector2(Position.X + PaddleWidthHalf - BallWidthHalf, Position.Y - BallHeight);
		}
	}

	public void AddStartingBall(ball ball)
	{
		startingBall = ball;
		move();
	}

	public void RemoveStartingBall()
	{
		if (startingBall != null)
		{
			startingBall.QueueFree();
			startingBall = null;
			move();
		}
	}
	private void RemoveGoodieTimer_Elapsed()
	{
		IsGoodieActive = null;
		allGoodieNodes.ForEach(n => n.Visible = false);
	}

	internal void AddGoodie(GoodieDrop goodieDrop)
	{
		switch (goodieDrop.BlockType)
		{
			case BlockTypes.Rocket:
				addRockets();
				break;
			case BlockTypes.BallMagnet:
				addBallMagnet();
				break;
		}
	}

	private void addRockets()
	{
		GD.Print("AddRockets");

		allGoodieNodes.ForEach(n => n.Visible = false);
		GetNode<Node2D>("Rockets").Visible = true;
		IsGoodieActive = BlockTypes.Rocket;

		RemoveGoodieTimer.Stop();
		RemoveGoodieTimer.Start();
	}

	private void addBallMagnet()
	{
		GD.Print("AddMagnets");

		allGoodieNodes.ForEach(n => n.Visible = false);
		GetNode<Node2D>("Magnets").Visible = true;
		IsGoodieActive = BlockTypes.BallMagnet;

		RemoveGoodieTimer.Stop();
		RemoveGoodieTimer.Start();
	}
}
