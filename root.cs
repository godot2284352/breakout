using Breakout;
using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using static Godot.TextServer;

public partial class root : Node2D
{
	private Label labelBallPos;
    private Label labelRocketTimer;
    private paddle paddle;
	private ball ball;


	public override void _Ready()
	{
		labelBallPos = GetNode<Label>("HUD/LabelBallVelocity");
		labelRocketTimer = GetNode<Label>("HUD/LabelGoodiesTimer");
		paddle = GetNode<paddle>("Paddle");

		loadLevel(getDebugLevel.GetDebugLevel());
		createStartingBall();

		QueueRedraw();
		// Input.MouseMode = Input.MouseModeEnum.Hidden;

		// paddle.AddRockets();
	}

	public override void _Process(double delta)
	{
		labelBallPos.Text = $"BallVelocity: {ball?.Velocity}";
		labelRocketTimer.Text = $"GoodieTimer: {(int)paddle.RemoveGoodieTimer.TimeLeft}";


		if (Input.IsActionJustPressed("DebugResetBall"))
		{
			if (ball != null)
			{
				paddle.RemoveStartingBall();
				ball.QueueFree();
				RemoveChild(ball);
			}
			createStartingBall();
		}
	}

	private void createStartingBall()
	{
		ball = ResourceLoader.Load<PackedScene>("res://ball.tscn").Instantiate<ball>();

		AddChild(ball);
		paddle.AddStartingBall(ball);
	}

    private void loadLevel(LevelInfo level)
	{
		var blockScene = ResourceLoader.Load<PackedScene>("res://block.tscn");
		var blockSize = blockScene.Instantiate().GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size;
		var marginBetweenBlocks = new Vector2(1, 1);

		var rowPosition = 300.0f;
		var levelWidth = level.Blocks[0].Length * (blockSize.X + marginBetweenBlocks.X);
		var startX = GetViewport().GetVisibleRect().Size.X / 2.0f - (levelWidth / 2.0f);

		for (var row = 0; row < level.Blocks.Length; row++)
		{
			var blockX = startX;
			var lvlRow = level.Blocks[row];

			for (var col = 0; col < lvlRow.Length; col++)
			{
				var block = blockScene.Instantiate<block>();
				var lvlCol = lvlRow[col];

				block.BlockType = lvlCol.BlockType;
				block.Color = lvlCol.Color;

				block.Position = new Vector2(blockX, rowPosition);

				blockX += blockSize.X + marginBetweenBlocks.X;

				AddChild(block);
			}

			rowPosition += blockSize.Y + marginBetweenBlocks.Y;
		}
	}
}
