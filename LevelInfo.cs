﻿using Godot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Breakout
{
	internal class LevelInfo
	{
		public string Name { get; set; }
		public BlockInfo[][] Blocks { get; set; }
	}

	internal class BlockInfo
	{
		public Color Color { get; set; }
		public BlockTypes BlockType { get; set; }

		public BlockInfo()
		{

		}

		public BlockInfo(BlockTypes blockType, Color? color = null)
		{
			Color = color ?? Colors.White;
			BlockType = blockType;
		}
	}
}
