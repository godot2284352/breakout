using Godot;
using System;

namespace Breakout;

public partial class GoodieDrop : CharacterBody2D
{
	[Export]
	public BlockTypes BlockType { get; set; }

	[Export]
	public Color Color { get; set; } = Colors.White;

	public const float DefaultVelocity = 100.0f;

	public override void _Ready()
	{
		var sprite = GetNode<Sprite2D>("Sprite2D");

		switch (BlockType)
		{
			case BlockTypes.Rocket:
				sprite.Texture = ResourceLoader.Load<Texture2D>("res://assets/blocks/goodies/rocket.png");
				break;
			case BlockTypes.BallMagnet:
				sprite.Texture = ResourceLoader.Load<Texture2D>("res://assets/blocks/goodies/ballmagnet.png");
				break;

			case BlockTypes.Normal:
			default:
				break;

		}

		Visible = false;
		Modulate = Color == default ? Colors.White : Color;
	}

	public void Drop()
	{
		Visible = true;
		Velocity = new Vector2(0.0f, DefaultVelocity);
	}

	public override void _PhysicsProcess(double delta)
	{
		var collInfo = MoveAndCollide(Velocity * (float)delta);
		if (collInfo != null)
		{
			var collider = collInfo.GetCollider();

			if (collider is paddle paddle)
			{
				GD.Print($"Goodie collided with paddle {this}");

				paddle.AddGoodie(this);
				QueueFree();
			}
			if (collider is Wall)
			{
				GD.Print($"Goodie collided with Wall {this}");
				QueueFree();
			}

			// GD.Print($"Goodie Collided width {collInfo} {collider}");
		}
	}

	public override string ToString() => $"Goodie {BlockType}; Color: {Color}; Pos: {Position}";
}
