using Godot;
using System;

[Tool]
public partial class Walls : Node2D
{
	// Adapted from https://www.reddit.com/r/godot/comments/ayz2mj/here_is_a_simple_solution_for_automatic_window/

	[Export]
	public float Thickness { get; set; } = 0f;

	[Export]
	public Color ColorLeft { get; set; } = default;
	[Export]
	public Color ColorRight { get; set; } = default;
	[Export]
	public Color ColorTop { get; set; } = default;
	[Export]
	public Color ColorBottom { get; set; } = default;

	public override void _Ready()
	{
		var windowSize = GetViewport().GetVisibleRect().Size;
		var thicknessHalf = Thickness / 2f;

		if (Engine.IsEditorHint())
		{
			// GetViewport gibt im Editor den Viewport des kompletten Fensters, somit sind die Wände dann außerhalb des Spielbereichs.
			windowSize = new Vector2((int)ProjectSettings.GetSetting("display/window/size/viewport_width"), (int)ProjectSettings.GetSetting("display/window/size/viewport_height"));
		}

		GD.Print($"Walls: WindowSize: {windowSize}; Thickness: {Thickness}");

		addWall(new Vector2(Thickness, windowSize.Y), new Vector2(thicknessHalf, windowSize.Y / 2), ColorLeft);
		addWall(new Vector2(Thickness, windowSize.Y), new Vector2(windowSize.X - thicknessHalf, windowSize.Y / 2), ColorRight);
		addWall(new Vector2(windowSize.X, Thickness), new Vector2(windowSize.X / 2, thicknessHalf), ColorTop);
		
		var bottom = addWall(new Vector2(windowSize.X, Thickness), new Vector2(windowSize.X / 2, windowSize.Y - thicknessHalf), ColorBottom);
		bottom.IsGameOver = true;
	}

	private Wall addWall(Vector2 size, Vector2 transform, Color color)
	{
		var wall = new Wall();

		var shape = new RectangleShape2D
		{
			Size = size,
		};

		var collShape = new CollisionShape2D()
		{
			Transform = new Transform2D(0, transform),
			Shape = shape,
		};

		wall.AddChild(collShape);
		AddChild(wall);

		/*
				var shapeowner = CreateShapeOwner(shape);
				ShapeOwnerSetTransform(shapeowner, new Transform2D(0, transform));
				ShapeOwnerAddShape(shapeowner, shape);
		*/

		if (color != default)
		{
			var colorRect = new ColorRect()
			{
				Position = new Vector2(transform.X - (size.X / 2f), transform.Y - (size.Y / 2f)),
				Size = shape.GetRect().Size,
				Color = color,
			};
			AddChild(colorRect);
		}

		return wall;
	}
}

public partial class Wall : StaticBody2D
{
	public bool IsGameOver { get; set; } = false;
}
