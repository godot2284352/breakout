# TODO

## Features

* Noch mal von vorne anfangen und alles "neu"!!!
* ResourceType
	* Für Goodies z.B.
	* Wichtig [GlobalClass] als Class-Attribute!

* Die Velocity-Adjust Blöcke sollen nicht den Ball direkt ändern, sondern das auch als Goodie droppen
    * ( dann geht auch der Bug weg )
    * Problem: Das Goodie hat keinen Zugrif auf die Bälle
        * a) Root muss goodies kennen. Dann Goodie Event AdjustBalls.
        * b) Goodie muss root kennen. root.AdjustBalls.
        * c) statt root eine List<ball> übergeben.
        * d) **BallSpawner komponente**?! ( Siehe unten )
* Extra Ball Goodie
    * Wenn aufgesammelt hat man einen weiteren Ball.
    * Der muss normal per Klick gestartet werden.
    * Hierfür muss ich umstellen von einem StartingBall auf List<Ball>
    * Vorher refactor?
* Paddle vergrößern Goodie
* Paddle verkleinern Goodie
* Game Over Blöcke. Wenn man die trifft, Ball tot.
* Bewegte Blöcke
	* hin und her bewegen <-> ( Unzerstörbar? )
	* Unzerstörbar. Wenn getroffen, dann bewegen sie sich an eine andere Stelle  <-->
* Ball anpassbar machen
	* Ball kann durch blöcke durchgehen, zerstört die blöcke, aber prallt nicht ab.
	* Ball prallt ab, aber lässt mehrere Blöcke explodieren ( wie beim Bombemblock )
* "Level" Scene weiter machen. => Ich möchte gerne im Editor mein Debug Level sehen.

* Punkte / UI Overlay
    * Es ergibt für mich keinen Sinn Punkte zu vergeben.
    * Es müssen ja eh immer alle Blöcke abgetroffen werden. Also wäre die Punktzahl am Ende eh immer gleich.
    * Außer man mach so besonderheiten wie:
        * Mehr punkte, wenn Blöcke innerhalb kurzer Zeit abgetroffen werden.
    * Das für mich einzig sinnvolle für Punkte ist wie lange man für das Level gebraucht hat.
* Level bauen. ( Zumindest 2-3 um ein Flow + Hauptmenü machen zu können )
* Hauptmenü
* 2 Spieler Modus
	a) jeder kann an jede Stelle
	b) jeder kann nur auf einer Seite bleiben.

## Bugs

* Wenn die Rakete auf einen Ball-Velocity-Adjust block trifft, wird der ball nicht adjustiert
    * Der Code für die adjustierung ist im ball.
    * Refactor ?!

## Sonstiges

* Block.Explode und Block.RemoveByHit ist Code doppelt!!
* Raketen und Magnetgoodie gleichzeitig nutzbar machen?
* on_area_body_entered(body)  if (body.hasfunction(function)) body.function();
    * if (body.hasfunction(SpawnGoodie)) body.SpawnGoodie
    * ( propagate_call(functionName) ruft in jeder Child-Node die Funktion auf, wenn sie existiert )
* Position = Position with { X = 100.0f };
* Ballspawner
```
Wohin mit dem Ballspawner
Wie können die Komponenten darauf zugreifen
Wie kommt er Ball immer ins Root
 Wenn der Ball eh immer ins Root muss,
  dann brauch ich auch kein Ballspawner, sondern kann eine
  Ball root.SpawnBall() machen, die als delegeate an alles übegeben werden kann.
  Das funzt mit Godot vermutlich aber nicht so, als dann doch so ne Komponente.
Autoload / singleton
  Eigentlich möchte ich das nicht nutzen..
  Und, ich muss dem Autoload dann auch im root immer das root zuweisen.
    ist auch iWie blöd, dann kann das root die auch gleich erstellen.

**Ich denke ich lasse root einfach die Bälle verwalten & spawnen.
  Root ist ja eigentlich das Level, also sollte das passen.**

Bälle müssen adjustiert werden durch
	a) Goodies
        Root Parent
        ( Goodie wird vom Block erstellt und übergibt es dem Parent ( root ) )
    b) Paddle
        Root Parent
	c) Blöcke
        Root parent
	d) Level / Timer
        Root
```

## Schöner machen

aus den blöcken ne polygon2d  

farbpalette nehmen  
 lospec.com  

world environment  
 mode = canvas für 2d  
rumspielen mit  
 tonemap  
 glow  

screenshake  
 ( mithilfe der kamera? )  

Licht  
 an den zwei seiten ne neonröhre  

Partikel  
 beim treffen / explodieren  
