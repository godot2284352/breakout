using Godot;
using System;

namespace Breakout;

[Tool]
public partial class Editor_script_faker : Node2D
{
	[Export]
	public bool CreateDebugLevel { get => false; set { if (value == true) { createDebugLevel(); } } }

	public override void _Ready()
	{
	}

	private void createDebugLevel()
	{
		GD.Print("You clicked me!");
		GD.Print($"Tree: {GetTree()}");
		GD.Print($"Tree EditedSceneRoot: {GetTree().EditedSceneRoot.Name}");
		GD.Print($"Edited Scene: {EditorInterface.Singleton.GetEditedSceneRoot().Name}");

		var blockScene = ResourceLoader.Load<PackedScene>("res://block.tscn");
		var blockSize = blockScene.Instantiate().GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size;
		var marginBetweenBlocks = new Vector2(1, 1);

		var block = blockScene.Instantiate<block>();

		block.Position = new Vector2(100, 50);

		EditorInterface.Singleton.GetEditedSceneRoot().AddChild(block);
		block.Owner = EditorInterface.Singleton.GetEditedSceneRoot();
	}

}
