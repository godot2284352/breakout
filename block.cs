using Breakout;
using Godot;
using System;
using System.ComponentModel;
using System.Linq;

[Tool]
public partial class block : StaticBody2D
{
	private Area2D explosionArea;
	private bool isExploding = false;
	private PackedScene goodieSzene;

	public float? VelocityAdjust { get; set; } = null;
	public GoodieDrop DropsGoodie { get; set; } = null;

	[Export]
	public BlockTypes BlockType { get; set; }

	[Export]
	public Color Color { get; set; } = Colors.White;

	public override void _Ready()
	{
		explosionArea = GetNode<Area2D>("ExplosionArea");
		goodieSzene = ResourceLoader.Load<PackedScene>("res://goodie_drop.tscn");

		setSprite();

		switch (BlockType)
		{
			case BlockTypes.Acceleration:
				VelocityAdjust = 1.5f;
				break;

			case BlockTypes.Deceleration:
				VelocityAdjust = 0.5f;
				break;

			case BlockTypes.Rocket:
			case BlockTypes.BallMagnet:
			case BlockTypes.Random:
				createGoodie();
				break;
		}

		Modulate = Color == default ? Colors.White : Color;
	}

	public override void _Draw()
	{
		// if (BlockType == BlockTypes.Bomb)
		// {
		// 	DrawArc(Vector2.Zero, ExplosionRadius, 0, 360, 360, Colors.Black);
		// }
	}

	private void setSprite()
	{
		var sprite = GetNode<Sprite2D>("OverlaySprite");

		if (BlockType == BlockTypes.Normal)
		{
			sprite.Texture = null;
		}
		else
		{
			var resourceName = $"res://assets/blocks/{BlockType.ToString().ToLower()}.png";

			GD.Print($"Loading Sprite {resourceName}");
			sprite.Texture = ResourceLoader.Load<Texture2D>(resourceName);
		}
	}

	public override string ToString()
		=> $"{BlockType}; Color: {Color}; DropsGoodie: {DropsGoodie}";

	internal void RemoveByHit()
	{
		GD.Print($"Block RemoveByHit {this}");

		if (BlockType == BlockTypes.Indestructible)
		{
			return;
		}

		if (BlockType == BlockTypes.Petrified2)
		{
			BlockType = BlockTypes.Petrified1;
			setSprite();
			return;
		}
		else if (BlockType == BlockTypes.Petrified1)
		{
			BlockType = BlockTypes.Normal;
			setSprite();
			return;
		}

		DropsGoodie?.Drop();

		if (BlockType == BlockTypes.Bomb)
		{
			GD.Print($"Collided with Bomb-Block: {this}");

			explode();
		}

		QueueFree();
	}

	private void explode()
	{
		GD.Print($"Block Explode {this}");

		if (BlockType == BlockTypes.Indestructible)
		{
			return;
		}


		isExploding = true;

		var explosionBlocks = explosionArea.GetOverlappingAreas().Where(a => a.Name == "BlockArea");
		explosionBlocks.ToList().ForEach(b =>
		{
			var block = b.GetParent() as block;

			if (block.BlockType == BlockTypes.Bomb && !block.isExploding)
			{
				block.explode();
			}

			if (block.BlockType == BlockTypes.Petrified2)
			{
				block.BlockType = BlockTypes.Petrified1;
				block.setSprite();
				return;
			}
			else if (block.BlockType == BlockTypes.Petrified1)
			{
				block.BlockType = BlockTypes.Normal;
				block.setSprite();
				return;
			}

			if (block.BlockType != BlockTypes.Indestructible)
			{
				block.QueueFree();
			}
		});

		// GD.Print($"Block.Explode(); {this}; Blocks: {explosionBlocks.Count()}");
	}


	private void createGoodie()
	{
		var goodie = goodieSzene.Instantiate<GoodieDrop>();

		goodie.BlockType = BlockType;
		goodie.Position = Position;

		if (BlockType == BlockTypes.Random)
		{
			var types = BlockTypesWithDropsWithoutRandom.Types;
			var randomGoodieType = types[GD.RandRange(0, types.Count - 1)];

			goodie.BlockType = randomGoodieType;
		}

		DropsGoodie = goodie;
		GD.Print($"Created GoodieDrop: {goodie}");

		GetParent().AddChild(goodie);
	}
}