using Godot;
using System;

public partial class Rocket : CharacterBody2D
{
	public const float Speed = 300.0f;

	public override void _PhysicsProcess(double delta)
	{
		Velocity = Vector2.Up * Speed;

		var collInfo = MoveAndCollide(Velocity * (float)delta);
		if (collInfo != null)
		{
			GD.Print($"Rocket collided with: {collInfo.GetCollider()}");

			if (collInfo.GetCollider() is block block)
			{
				QueueFree();
				block.RemoveByHit();
			}

			if (collInfo.GetCollider() is Wall wall)
			{
				QueueFree();
			}
		}
	}
}
