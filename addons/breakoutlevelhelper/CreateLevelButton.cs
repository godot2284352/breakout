﻿using Godot;
using System.Reflection.Emit;

[Tool]
public partial class CreateLevelButton : Button
{
	public override void _EnterTree()
	{
		Pressed += Clicked;
	}

	public void Clicked()
	{
		GD.Print("You clicked me!");
		GD.Print($"Tree: {GetTree()}");
		GD.Print($"Tree EditedSceneRoot: {GetTree().EditedSceneRoot.Name}");
		GD.Print($"Edited Scene: {EditorInterface.Singleton.GetEditedSceneRoot().Name}");

		var blockScene = ResourceLoader.Load<PackedScene>("res://block.tscn");
		var blockSize = blockScene.Instantiate().GetNode<CollisionShape2D>("CollisionShape2D").Shape.GetRect().Size;
		var marginBetweenBlocks = new Vector2(1, 1);

		var block = blockScene.Instantiate<block>();

		block.Position = new Vector2(100, 10);
	
		EditorInterface.Singleton.GetEditedSceneRoot().AddChild(block);
		block.Owner = EditorInterface.Singleton.GetEditedSceneRoot();
	}
}