#if TOOLS
using Godot;
using System;

[Tool]
public partial class breakoutlevelhelper : EditorPlugin
{
	private Button button = new CreateLevelButton();

	public override void _EnterTree()
	{
		AddControlToDock(DockSlot.LeftUl, button);
	}

	public override void _ExitTree()
	{
		RemoveControlFromDocks(button);
		button.Free();
	}
}
#endif
