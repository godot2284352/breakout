using Godot;
using System;

public partial class ball : CharacterBody2D
{
	public const float MaxVelocity = 400.0f;
	public const float MinVelocity = 100.0f;

	private bool isStarting = true;

	public override void _Ready()
	{
	}

	public override void _PhysicsProcess(double delta)
	{
		if (isStarting)
		{
			return;
		}

		var collInfo = MoveAndCollide(Velocity * (float)delta);
		if (collInfo != null)
		{
			var collider = collInfo.GetCollider();
		
			//GD.Print($"Ball Collided {collInfo} {collider}");

			if (collider is block block)
			{
				GD.Print($"Block: {block} {(collider as Node).Name}; VelAdj: {block.VelocityAdjust}; Velocity: {Velocity}; Max: {MaxVelocity}; Min: {MinVelocity}");

				var newVelocity = Velocity.Bounce(collInfo.GetNormal());
				
				if (block.VelocityAdjust != null)
				{
					// Wenn der Block schneller machen soll und   beide Richtungen noch nicht schneller sind als MAX
					if (block.VelocityAdjust > 1.0f && newVelocity.Abs().X < MaxVelocity && newVelocity.Abs().Y < MaxVelocity)
					{
						// Ball schneller machen
						newVelocity *= block.VelocityAdjust.Value;

						// Prüfen ob eine Richtung nun zu schnell ist
						var maxFromNewVelo = Math.Max(newVelocity.Abs().X, newVelocity.Abs().Y);

						GD.Print($"Checking MaxVel: NewVel: {newVelocity}; MaxFromNewVel: {maxFromNewVelo}; MaxVelo: {MaxVelocity}");
						
						if (maxFromNewVelo > MaxVelocity)
						{
							// Beide Richtungen in Relation langsamer bis MAX machen
							var diff = MaxVelocity / maxFromNewVelo;

							newVelocity.X *= diff;
							newVelocity.Y *= diff;

							GD.Print($"Checking MaxVel: Reached Max: NewVel: {newVelocity}; Diff: {diff}");
						}
					}

					// Ich verstehe aktuell nicht wirklich, wie der zweite Block funktioniert, aber es muss wohl so
					// Wenn der Block langsamer machen soll und     keine der Richtungen zu langsam ist
					if (block.VelocityAdjust < 1.0f && (newVelocity.Abs().X > MinVelocity || newVelocity.Abs().Y > MinVelocity))
					{
						newVelocity *= block.VelocityAdjust.Value;

						// Prüfen ob eine Richtung nun zu langsam ist
						var maxFromNewVelo = Math.Max(newVelocity.Abs().X, newVelocity.Abs().Y);
					
						GD.Print($"Checking MinVel: NewVel: {newVelocity}; MaxFromNewVel: {maxFromNewVelo}; MinVelocity: {MinVelocity}");
						
						if (maxFromNewVelo < MinVelocity)
						{
							// Beide Richtungen in Relation schneller bis MIN machen
							var diff = MinVelocity / maxFromNewVelo;

							newVelocity.X *= diff;
							newVelocity.Y *= diff;

							GD.Print($"Checking MinVel: Reached Min: NewVel: {newVelocity}; Diff: {diff}");
						}
					}
				}

				Velocity = newVelocity;

				block.RemoveByHit();
			}
			else if (collider is paddle paddle)
			{
				// Paddle
				//GD.Print($"Paddle Bounce: {collInfo.GetNormal()} {collInfo.GetPosition()} {collInfo.GetRemainder()} {collInfo.GetTravel()} {collInfo.GetAngle()} {collInfo.GetLocalShape()} {collInfo.GetColliderShape()} {collInfo.GetColliderVelocity()} {collInfo.GetDepth()}");

				if (paddle.IsGoodieActive == Breakout.BlockTypes.BallMagnet)
				{
					GD.Print("Magnet!");
					Stop();
					paddle.AddStartingBall(this);
				}
				else 
				{
					var colPos = collInfo.GetPosition();
					
					var posOnPaddle = paddle.ToLocal(colPos).X;
					var paddleMiddle = paddle.PaddleWidthHalf;

					var newVelX = Velocity.X;
					if (posOnPaddle > paddleMiddle)
					{
						if (newVelX < 0)
						{
							newVelX *= -1;
						}
					}
					else
					{
						if (newVelX > 0)
						{
							newVelX *= -1;
						}
					}
					var newVel = new Vector2(newVelX, Velocity.Y * -1);
					
					GD.Print($"Paddle Bounce: {colPos}          {posOnPaddle} {paddleMiddle}            {Velocity} -> {newVel}");
					Velocity = newVel;


					//Velocity = Velocity.Bounce(collInfo.GetNormal());
					// Bounce dreht quasi einfach nur das Vorzeichen um
					// (100, 300) -> (100, -300)
					// (150, 450) -> (150, -450)
				}


			}
			else if (collider is Wall wall)
			{
				if (wall.IsGameOver)
				{
					gameOver();
					return;
				}
				Velocity = Velocity.Bounce(collInfo.GetNormal());
			}
		}

		if (Input.IsActionJustPressed("DebugBallSchneller"))
		{
			Velocity *= 1.5f;
		}
		if (Input.IsActionJustPressed("DebugBallLangsamer"))
		{
			Velocity *= 0.5f;
		}
	}

	internal void Start(Vector2 velocity)
	{
		GD.Print($"Ball Start: {velocity}");
		isStarting = false;

		Velocity = velocity;
	}

	internal void Stop()
	{
		isStarting = true;

		Velocity = Vector2.Zero;
	}

	private void gameOver()
	{
		Velocity = Vector2.Zero;
		GD.Print($"Game Over 'Block'");
	}
}
